let name = prompt("What is your name?", " ");
let ageInput = prompt("How old are you?", " ");
let age = parseInt(ageInput);

if (!name || isNaN(age)) {
  alert("Please enter valid name and age!");
} else {
  if (age <= 18) {
    alert("You are not allowed to visit this website");
  } else if (age > 18 && age <= 22) {
    let success = confirm("Are you sure you want to continue?");
    if (success) {
      alert(`Welcome, ${name}`);
    } else {
      alert("You are not allowed to visit this website");
    }
  } else {
    alert(`Welcome, ${name}`);
  }
}
